export function conversionInfo(key, cap=false) {
  let data = {
    'foncee': 'foncée',
    'tres_claire': 'très claire',
    'boheme': 'bohème',
  };

  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  if (key in data){
    return cap ? capitalizeFirstLetter(data[key]) : data[key];
  }
  else {
    return cap ? capitalizeFirstLetter(key) : key;
  }
}

export let silyaData = {
  'morphologie': {
    'taille': ['grande', 'moyenne', 'petite'],
    'peau': ['foncee', 'mate', 'claire', 'tres_claire'],
    'yeux': ['bleu', 'vert', 'noisette', 'brun'],
    'visage': ['rond', 'ovale', 'carre', 'coeur'],
    'cou': ['long', 'moyen', 'petit'],
    'main': ['long', 'moyen', 'petit'],
  },
  'occasion': {
    'occasion': ['soiree', 'quotidien', 'travail'],
  },
  'tenue': {
    'couleur': [],
    'col': ['rond', 'v', 'cou', 'bustier'],
  }
};
