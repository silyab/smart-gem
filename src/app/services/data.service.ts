import { Injectable } from '@angular/core';
import {ApiService} from './api.service';

@Injectable()
export class DataService {

  user = {};
  changed = {'silya': false, 'filtres': false};

  constructor(private apiService: ApiService) {
    this.initiateId();
  }

  public updateUser(key, key2, value, save=false) {
    this.changed[key] = true;
    this.user[key][key2] = value;

    if (save){
      this.syncUser();
    }
  }

  public updateWishlist(bijou_id, add, size, callback=null){
    const data = {'bijou_id': bijou_id, 'add': add};
    let this_ = this;

    this.isready(function () {
      this_.apiService.updateWishlist(this_.user['id'], data).subscribe((response) => {
        this_.getWishlist(size, callback);
      }, (error) => {
        console.log('Impossible de modifier la wishlist, error : ', error);
        this_.monitorError();
      });
    });
  }

  public whenInfoReady(){
    let _this = this;
    return new Promise(function(resolve, reject) {
        setTimeout(function(){
      if (_this.user.hasOwnProperty('silya')) {
        resolve();
      } else {
        _this.whenInfoReady().then(function(response) {resolve()});
      }}, 200);
    });
  }

  public getWishlist(size, callback=null) {
    let _this = this;
    return this.isready(function () {_this.syncUser(_this.pullWishlist(size, callback))});
  }

  public getSelection(tag, size) {
    let _this = this;
    return new Promise(function(resolve, reject) {
      _this.apiService.getSelection(tag, size).subscribe((response) => {
        resolve(response);
      }, (error) => {
        console.log('Impossible de recuperer selection, error : ', error);
        _this.monitorError();
        reject();
      });
    });
  }

  public getSelectionUser(callback=null) {
    let _this = this;
    return this.isready(function () {_this.syncUser(_this.pullSelectionUser(callback))});
  }

  public getBijou(bijou_id, callback=null) {
    let _this = this;
    return this.isready(function () {_this.syncUser(_this.pullBijou(bijou_id, callback))});
  }

  public emailWishlist(email, callback=null) {
    let _this = this;
    this.isready(function () {_this.syncUser(_this.sendEmail(email, callback))});
  }

  private isready(callback) {
    if("id" in this.user){
      return callback();
    }
    else{
      let _this = this;
      setTimeout(function(){_this.isready(callback)}, 250);
    }
  }

  private initiateId() {
    this.user['id'] = localStorage.getItem('clientID');

    /* Nouvel id si pas d'id*/
    if (this.user['id'] === null){
      this.askNewId();
    }
    /* Test si l'id toujours valable */
    else {
      this.apiService.testId(this.user['id']).subscribe((response) => {
        if (response.hasOwnProperty('error')) {
          /* Si not found on demande un nouvel id */
          if (response.error === 'not found'){
            console.log('Id plus valable demande d\'un nouveau...');
            this.askNewId();
          }
          /* autre erreur */
          else {
            console.log('Le serveur a rencontré une erreur avec mon id');
            this.monitorError();
          }
        }
        else {
          this.initiateUser();
        }
      }, (error) => {
        console.log('Impossible de contacter le serveur : ' + error);
        this.monitorError();
      });
    }
  }

  private askNewId() {
    this.apiService.getId().subscribe((response) => {
      this.user['id'] = response;
      this.apiService.createUser(this.user['id']).subscribe((response) => {
        if(response.hasOwnProperty('error')){
          console.log('Impossible de créer un nouvel utilisateur, error : ', response['error']);
          this.monitorError();
        }
        else{
          localStorage.setItem('clientID', this.user['id']);
          this.initiateUser();
        }
      }, (error) => {
        console.log('Impossible de créer un nouvel utilisateur, error : ', error);
        this.monitorError();
      });
    }, (error) => {
      console.log('Impossible de créer un nouvel utilisateur, error : ', error);
      this.monitorError();
    });
  }

  private initiateUser() {
    this.apiService.getInfo(this.user['id']).subscribe((response) => {
      this.user['silya'] = response['silya'];
      this.user['filtres'] = response['filtres'];
    }, (error) => {
      console.log('Impossible de récupérer infos, error : ', error);
      this.monitorError();
    });
  }

  private syncUser(callback=null) {
    let data = {};
    let send = false;

    if( this.changed['silya']){ data['silya'] = this.user['silya']; send = true; this.changed['silya'] = false;}
    if( this.changed['filtres']){ data['filtres'] = this.user['filtres']; send = true; this.changed['filtres'] = false;}

    if (send) {
      this.apiService.updateInfo(this.user['id'], data).subscribe((response) => {
        if (callback instanceof Function) { callback(); }
      }, (error) => {
        console.log('Impossible de synchroniser infos, error : ', error);
        this.monitorError();
      });
    }
  }

  private pullWishlist(size, callback=null){
    this.apiService.getWishlist(this.user['id'], size).subscribe((response) => {
      this.user['wishlist'] = response;
      if (callback instanceof Function) { callback(); }
    }, (error) => {
      console.log('Impossible de synchroniser infos, error : ', error);
      this.monitorError();
    });
  }

  private pullSelectionUser(callback=null) {
    this.apiService.getUserSelection(this.user['id']).subscribe((response) => {
      this.user['selection'] = response;
      if (callback instanceof Function) { callback(); }
    }, (error) => {
      console.log('Impossible de recuperer selection, error : ', error);
      this.monitorError();
    });
  }

  private pullBijou(bijou_id, callback=null) {
    this.apiService.getBijou(this.user['id'], bijou_id).subscribe((response) => {
      this.user['bijou'] = response;
      if (callback instanceof Function) { callback(); }
    }, (error) => {
      console.log('Impossible de recuperer info bijou, error : ', error);
      this.monitorError();
    });
  }

  private sendEmail(email, callback=null) {
    this.apiService.emailWishlist(this.user['id'], {'email':email}).subscribe((response) => {
      if (callback instanceof Function) { callback(); }
    }, (error) => {
      console.log('Impossible d envoyer email, error : ', error);
      this.monitorError();
    });
  }

  private monitorError() {

  }
}
