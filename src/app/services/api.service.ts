import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {
  ip = environment.ip;

  basic_header = new Headers();

  constructor(private _http: Http) {
    this.basic_header.append('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
    this.basic_header.append('dataType-Type', 'json');
  }

  // User
  getId() {
    const url = 'http://' + this.ip + '/user_id';

    return this._http
      .get(url)
      .map(res => res.json());
  }

  testId(id) {
    const url = 'http://' + this.ip + '/user/' + id;

    return this._http
      .get(url)
      .map(res => res.json());
  }

  createUser(id) {
    const url = 'http://' + this.ip + '/user/' + id;

    return this._http
      .post(url, '', {headers: this.basic_header})
      .map(res => res.json());
  }

  getInfo(id) {
    const url = 'http://' + this.ip + '/user/' + id + '/info';

    return this._http
      .get(url)
      .map(res => res.json());
  }

  updateInfo(id, data) {
    const url = 'http://' + this.ip + '/user/' + id + '/info';

    return this._http
      .post(url, JSON.stringify(data), {headers: this.basic_header})
      .map(res => res.json());
  }

  // Selection
  getSelection(tag, size) {
    const url = 'http://' + this.ip + '/selection/' + tag + '/' + size;

    return this._http
      .get(url)
      .map(res => res.json());
  }

  getUserSelection(id) {
    const url = 'http://' + this.ip + '/user/' + id + '/selection';

    return this._http
      .get(url)
      .map(res => res.json());
  }

  // Bijou
  getBijou(id, bijou_id) {
    const url = 'http://' + this.ip + '/user/' + id + '/bijou/' + bijou_id;

    return this._http
      .get(url)
      .map(res => res.json());
  }

  // Wish list
  getWishlist(id, size) {
    const url = 'http://' + this.ip + '/user/' + id + '/wishlist/' + size;

    return this._http
      .get(url)
      .map(res => res.json());
  }

  updateWishlist(id, data) {
    const url = 'http://' + this.ip + '/user/' + id + '/wishlist/size';

    return this._http
      .post(url, JSON.stringify(data), {headers: this.basic_header})
      .map(res => res.json());
  }

  emailWishlist(id, data) {
    const url = 'http://' + this.ip + '/user/' + id + '/wishlist/email';

    return this._http
      .post(url, JSON.stringify(data), {headers: this.basic_header})
      .map(res => res.json());
  }

}
