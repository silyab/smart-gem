import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SilyaQuestionComponent } from './silya-question.component';

describe('SilyaQuestionComponent', () => {
  let component: SilyaQuestionComponent;
  let fixture: ComponentFixture<SilyaQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SilyaQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SilyaQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
