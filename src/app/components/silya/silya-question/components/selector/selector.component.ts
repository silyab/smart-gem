import {Component, Input, OnInit} from '@angular/core';
import {conversionInfo, silyaData} from "../../../../../services/meta";

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.css']
})
export class SelectorComponent implements OnInit {

  @Input('parent') parent;
  @Input('key') key;

  data = {
    'taille': {'titre': 'Taille'},
    'peau': {'titre': 'Couleur de peau'},
    'yeux': {'titre': 'Couleurs des yeux'},
    'visage': {'titre': 'Forme du visage'},
    'cou': {'titre': 'Longueur du cou'},
    'main': {'titre': 'Taille de la main'},
    'occasion': {'titre': 'Occasion de port du bijou'},
    'col': {'titre': 'Type de col'},
  };

  titre;
  selectors = [];
  selected;

  constructor() { }

  ngOnInit() {
    if (this.data.hasOwnProperty(this.key)){
      this.titre = this.data[this.key]['titre'];
      for (let elm of silyaData[this.parent.key][this.key]){
        this.selectors.push({'key': elm, 'titre': conversionInfo(elm, true)});
      }
    }
  }

  change(value){
    this.parent.change(this.key, value);
    this.selected = value;
  }

}
