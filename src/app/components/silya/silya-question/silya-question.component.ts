import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {silyaData} from "../../../services/meta";
import {DataService} from "../../../services/data.service";

@Component({
  selector: 'app-silya-question',
  templateUrl: './silya-question.component.html',
  styleUrls: ['./silya-question.component.css']
})
export class SilyaQuestionComponent implements OnInit {

  key;
  questions = [];
  titre = {
    'morphologie': 'Ma Morphologie',
    'occasion': 'Occasion',
    'tenue': 'Tenue',
  };

  constructor(private activatedRoute: ActivatedRoute, private dataService: DataService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      if (params['key'] !== undefined) {
        this.init(params['key'])
      }
      else {

      }
    });
  }

  init(key){
    this.key = key;
    for (let elm in silyaData[key]){
      if ( silyaData[key].hasOwnProperty(elm))
        this.questions.push(elm);
    }
  }

  change(key, value){
    let data = this.dataService.user['silya'][this.key];
    data[key] = value;
    this.dataService.updateUser('silya', this.key, data);
  }

}
