import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SilyaReglageComponent } from './silya-reglage.component';

describe('SilyaReglageComponent', () => {
  let component: SilyaReglageComponent;
  let fixture: ComponentFixture<SilyaReglageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SilyaReglageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SilyaReglageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
