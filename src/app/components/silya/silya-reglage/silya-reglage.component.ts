import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../services/data.service";
declare let $ :any;

@Component({
  selector: 'app-silya-reglage',
  templateUrl: './silya-reglage.component.html',
  styleUrls: ['./silya-reglage.component.css', '../silya-accueil/silya-accueil.component.css']
})
export class SilyaReglageComponent implements OnInit {

  silya;
  activated = [];
  isActif = {'morphologie': false, 'style': false, 'occasion': false, 'tenue': false, };

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.floatMenu();
    this.getSilya();
  }

  getSilya(){
    // chargement des reglages précédents si existant
    let _this = this;
    this.dataService.whenInfoReady().then(function () {
      _this.silya = _this.dataService.user['silya'];
      _this.getActivated();
    });
  }

  getActivated(){
    for (let key of this.silya){
      if (this.silya.hasOwnProperty(key)){
        if (this.silya[key]['actif']){
          this.activated.push(key);
          this.isActif[key] = true;
        }
      }
    }
  }

  activate(key, add){
    let old_data = this.dataService.user['silya'][key];

    if (add) {
      this.activated.push(key);
      this.isActif[key] = true;
      old_data['actif'] = true;
    }
    else {
      this.activated.splice(this.activated.indexOf(key), 1);
      this.isActif[key] = false;
      old_data['actif'] = false;
    }

    this.dataService.updateUser('silya', key, old_data, true);
  }

  floatMenu(){
    // flotaison du menu
    let container = $('#float-footer');
    let offset = $(window).outerHeight() - container.outerHeight() + 30;
    let limit = $('#stop-scroll');
    let minTop = limit.offset().top + limit.outerHeight() + container.outerHeight() + 150 - offset;
    let maxTop = $('#footer').offset().top - container.outerHeight() - 100;

    $(document).scroll(function() {
      container.css('top', Math.min( Math.max(minTop, $(document).scrollTop()), maxTop )+offset);
    });
  }

}
