import {Component, Input, OnInit} from '@angular/core';
import {DataService} from "../../../../../services/data.service";
import {conversionInfo, silyaData} from "../../../../../services/meta";

@Component({
  selector: 'app-filtre',
  templateUrl: './filtre.component.html',
  styleUrls: ['./filtre.component.css']
})
export class FiltreComponent implements OnInit {

  @Input('parent') parent;
  @Input('key') key;

  data = {
    'morphologie': {
      'titre': 'Morphologie',
      'presentation': 'Grâce à ce filtre Silya pourra te conseiller des bijoux qui correspondent le mieux à ton teint, la couleur de tes yeux, la forme de ton visage, etc.',
      'reglage': [{'key': 'Taille', 'value': '-'}, {'key': 'Peau', 'value': '-'}, {'key': 'Yeux', 'value': '-'}, {'key': 'Visage', 'value': '-'}, ],
      'suspension': true,
      'img': 'icon_lg_claire.png',
    },
    'style': {
      'titre': 'Style',
      'presentation': 'Grâce à ce filtre Silya analysera tes goûts pour te proposer des styles de bijoux qui te correspondent le mieux.',
      'reglage': [{'key': 'Styles préférés', 'value': '-'} ],
      'suspension': false,
      'img': 'icon_lg_style.png',
    },
    'occasion': {
      'titre': 'Occasion',
      'presentation': 'Active ce filtre si tu recherches des bijoux pour une occasion en particulier (occasion spéciale, au quotidien, travail, ...)',
      'reglage': [{'key': 'Occasion', 'value': '-'} ],
      'suspension': false,
      'img': 'icon_lg_occasion.png',
    },
    'tenue': {
      'titre': 'Tenue',
      'presentation': 'Active ce filtre si tu recherches des bijoux pour aller avec une tenue en particulier',
      'reglage': [{'key': 'Couleur', 'value': '-'}, {'key': 'Type de col', 'value': '-'}],
      'suspension': false,
      'img': 'icon_lg_tenue.png',
    },
  };

  titre;
  presentation;
  reglage = [];
  suspension;
  img;
  activable = true;

  constructor(private dataService:DataService) {

  }

  ngOnInit() {
    // initialisation des valeurs selon key
    for (let key of ['titre', 'presentation', 'reglage', 'suspension', 'img']){
      this[key] = this.data[this.key][key];
    }

    // chargement des reglages précédents si existant
    let _this = this;
    this.dataService.whenInfoReady().then(function () {

      //let silya = {'style': {'reglage':['boheme', 'classique', 'moderne'], 'actif': true}};
      let silya = _this.dataService.user['silya'];

      _this.testActivable(silya);

      if (_this.key == 'morphologie' && silya.hasOwnProperty('morphologie')) {

        _this.reglage[0]['value'] = silya['morphologie']['reglage'].hasOwnProperty('taille') ?
          conversionInfo(silya['morphologie']['reglage']['taille']) : '-';
        _this.reglage[1]['value'] = silya['morphologie']['reglage'].hasOwnProperty('peau') ?
          conversionInfo(silya['morphologie']['reglage']['peau']) : '-';
        _this.reglage[2]['value'] = silya['morphologie']['reglage'].hasOwnProperty('yeux') ?
          conversionInfo(silya['morphologie']['reglage']['yeux']) : '-';
        _this.reglage[3]['value'] = silya['morphologie']['reglage'].hasOwnProperty('visage') ?
          conversionInfo(silya['morphologie']['reglage']['visage']) : '-';

        if (silya['morphologie']['reglage'].hasOwnProperty('peau')) {
          _this.img = 'icon_lg_'+silya['morphologie']['reglage']['peau']+'.png';
        }
      }

      if (_this.key == 'style' && silya.hasOwnProperty('style') && silya['style']['reglage'].length > 0) {

        _this.reglage[0]['value'] =  conversionInfo(silya['style']['reglage'][0]);
        for (let i = 1; i < silya['style']['reglage'].length; i++){
          _this.reglage[0]['value'] += ', '+conversionInfo(silya['style']['reglage'][i]);
        }
      }

      if (_this.key == 'occasion' && silya.hasOwnProperty('occasion')) {
        _this.reglage[0]['value'] = conversionInfo(silya['occasion']['reglage']);
      }

      if (_this.key == 'tenue' && silya.hasOwnProperty('tenue')) {

        _this.reglage[0]['value'] = silya['tenue']['reglage'].hasOwnProperty('couleur') ?
          conversionInfo(silya['tenue']['reglage']['couleur']) : '-';
        _this.reglage[1]['value'] = silya['tenue']['reglage'].hasOwnProperty('col') ?
          conversionInfo(silya['tenue']['reglage']['col']) : '-';
      }
    });

  }

  testActivable(silya){
    if( this.key == 'style' && (!silya.hasOwnProperty(this.key) || silya['style']['reglage'].length == 0)){
      this.activable = false;
    }
    else {
      for (let key in silyaData[this.key]){
        if (silyaData[this.key].hasOwnProperty(key) &&
          (!silya.hasOwnProperty(this.key) || !silya[this.key]['reglage'].hasOwnProperty(key))){
          this.activable = false;
          break;
        }
      }
    }
  }

}
