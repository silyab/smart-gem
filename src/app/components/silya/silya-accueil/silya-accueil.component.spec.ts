import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SilyaAccueilComponent } from './silya-accueil.component';

describe('SilyaAccueilComponent', () => {
  let component: SilyaAccueilComponent;
  let fixture: ComponentFixture<SilyaAccueilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SilyaAccueilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SilyaAccueilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
