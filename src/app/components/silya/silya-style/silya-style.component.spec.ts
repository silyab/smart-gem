import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SilyaStyleComponent } from './silya-style.component';

describe('SilyaStyleComponent', () => {
  let component: SilyaStyleComponent;
  let fixture: ComponentFixture<SilyaStyleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SilyaStyleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SilyaStyleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
