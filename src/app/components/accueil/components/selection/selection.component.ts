import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../../services/data.service";

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.css', '../../accueil.component.css']
})
export class SelectionComponent implements OnInit {

  selections;
  hover;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    let _this = this;
    this.dataService.getSelection('accueil', 'short').then(function(response) {
      _this.selections = response;
    }, function(error) {
      console.error("Failed!", error);
    })
    ;
  }

}
