import { Component, OnInit } from '@angular/core';
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-bijouterie',
  templateUrl: './bijouterie.component.html',
  styleUrls: ['./bijouterie.component.css']
})
export class BijouterieComponent implements OnInit {

  constructor(private dataService: DataService) { }

  ngOnInit() {
  }

}
