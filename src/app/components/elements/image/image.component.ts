import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit, OnChanges  {
  @Input('id') id;
  @Input('image') image;
  @Input('ratio') ratio;
  @Input('max_hauteur') max_hauteur;
  @Input('hover') hover;
  @Input('classe') css_class;

  current_ratio;

  constructor() { }

  ngOnInit() {
    if (! this.ratio){
      this.ratio = this.max_hauteur/300;
    }
    this.current_ratio = this.ratio;
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.hover.currentValue == this.id){
      this.current_ratio = this.ratio * 1.1;
    }
    else {
      this.current_ratio = this.ratio;
    }
  }

}
