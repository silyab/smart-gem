import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {
  transform(value: string, max_length: number) : string {
    if (!value) return '';
    return value.length > max_length ? value.substring(0, max_length-3) + '...' : value;
  }
}
