import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { AccueilComponent } from './components/accueil/accueil.component';
import { BijouterieComponent } from './components/bijouterie/bijouterie.component';
import { ProduitComponent } from './components/produit/produit.component';
import { WhishlistComponent } from './components/whishlist/whishlist.component';
import { SilyaAccueilComponent } from './components/silya/silya-accueil/silya-accueil.component';
import { SilyaReglageComponent } from './components/silya/silya-reglage/silya-reglage.component';
import { DataService } from './services/data.service';
import { ApiService } from './services/api.service';
import { SelectionComponent } from './components/accueil/components/selection/selection.component';
import { ImageComponent } from './components/elements/image/image.component';
import { TruncatePipe } from "./styles/pipes";
import { SilyaQuestionComponent } from './components/silya/silya-question/silya-question.component';
import { FiltreComponent } from './components/silya/silya-reglage/components/filtre/filtre.component';
import { SilyaStyleComponent } from './components/silya/silya-style/silya-style.component';
import { SelectorComponent } from './components/silya/silya-question/components/selector/selector.component';

const appRoutes: Routes = [
  { path: '', component: AccueilComponent },
  { path: 'bijouterie', component: BijouterieComponent },
  { path: 'silya/question/:key', component: SilyaQuestionComponent },
  { path: 'silya/reglage', component: SilyaReglageComponent },
  { path: 'silya', component: SilyaAccueilComponent },
  { path: '**', redirectTo:'', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    BijouterieComponent,
    ProduitComponent,
    WhishlistComponent,
    SilyaAccueilComponent,
    SilyaReglageComponent,
    SelectionComponent,
    ImageComponent,
    TruncatePipe,
    SilyaQuestionComponent,
    FiltreComponent,
    SilyaStyleComponent,
    SelectorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [DataService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
