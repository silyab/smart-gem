import { SmartGemmePage } from './app.po';

describe('smart-gemme App', () => {
  let page: SmartGemmePage;

  beforeEach(() => {
    page = new SmartGemmePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
